using System;

namespace DDD
{
    public interface IDomainEvent
    {
        Guid AggregateId { get; set; }
        int Version { get; set; }
        DateTimeOffset TimeStamp { get; }
    }
}