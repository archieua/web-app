﻿using System;
using System.Collections.Generic;

namespace DDD
{
    public abstract class AggregateBase : IAggregate
    {
        private readonly List<IDomainEvent> pendingEvents = new List<IDomainEvent>();

        public Guid Id { get; protected set; }
        public int Version { get; private set; } = -1;

        void IAggregate.ApplyEvent(object @event)
        {
            ((dynamic)this).Apply((dynamic)@event);
            Version++;
        }

        public ICollection<IDomainEvent> GetPendingEvents()
        {
            return pendingEvents;
        }

        public void ClearPendingEvents()
        {
            pendingEvents.Clear();
        }

        protected void RaiseEvent(IDomainEvent @event)
        {
            ((IAggregate)this).ApplyEvent(@event);
            pendingEvents.Add(@event);
        }

        public abstract string Identifier { get; }
    }
}
