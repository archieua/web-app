﻿using System;
using System.Collections.Generic;

namespace DDD
{
    public abstract class DomainRepositoryBase : IDomainRepository
    {
        public abstract void Save<TAggregate>(TAggregate aggregate, bool isInitial = false)
            where TAggregate : IAggregate;

        public abstract TAggregate GetById<TAggregate>(Guid id) where TAggregate : IAggregate, new();

        public abstract TAggregate GetById<TAggregate>(Guid id, int version) where TAggregate : IAggregate, new();

        protected TAggregate BuildAggregate<TAggregate>(IEnumerable<IDomainEvent> events) where TAggregate : IAggregate, new()
        {
            var result = new TAggregate();
            foreach (var @event in events)
            {
                result.ApplyEvent(@event);
            }
            return result;
        }

        protected int CalculateExpectedVersion<T>(IAggregate aggregate, List<T> events)
        {
            return aggregate.Version - events.Count;
        }
    }
}
