﻿using System;

namespace DDD
{
    public interface IDomainRepository
    {
        void Save<TAggregate>(TAggregate aggregate, bool isInitial = false) where TAggregate : IAggregate;

        TAggregate GetById<TAggregate>(Guid id) where TAggregate : IAggregate, new();

        TAggregate GetById<TAggregate>(Guid id, int version) where TAggregate : IAggregate, new();
    }
}
