﻿using System;
using Paramore.Brighter;

namespace Delikasee.ProductCatalog.Commands
{
    public class ChangeProductPrimaryImage : IRequest
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string ImageUrl { get; set; }
    }
}
