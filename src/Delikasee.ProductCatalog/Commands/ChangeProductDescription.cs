﻿using System;
using Paramore.Brighter;

namespace Delikasee.ProductCatalog.Commands
{
    public class ChangeProductDescription : IRequest
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string Description { get; set; }
    }
}
