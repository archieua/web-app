﻿using System;
using Paramore.Brighter;

namespace Delikasee.ProductCatalog.Commands
{
    public class CreateProduct : IRequest
    {
        public Guid Id { get; set; }
        public Guid ProductId { get; set; }
        public string Name { get; set; }
    }
}
