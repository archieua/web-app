﻿using System;
using DDD;

namespace Delikasee.ProductCatalog.Events
{
    public class ProductCreated : BaseDomainEvent
    {
        public string Name { get; set; }

        public ProductCreated(Guid productId, string name)
        {
            AggregateId = productId;
            Name = name;
        }
    }
}
