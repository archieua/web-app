﻿using System;
using Paramore.Brighter;
using SimpleInjector;

namespace Delikasee.Core.Infrastructure.SimpleInjectorExt.Brighter
{
    public class SimpleInjectorMessageMapperFactory : IAmAMessageMapperFactory
    {
        private readonly Container container;

        public SimpleInjectorMessageMapperFactory(Container container)
        {
            this.container = container;
        }

        public IAmAMessageMapper Create(Type messageMapperType)
        {
            return (IAmAMessageMapper)container.GetInstance(messageMapperType);
        }
    }
}
