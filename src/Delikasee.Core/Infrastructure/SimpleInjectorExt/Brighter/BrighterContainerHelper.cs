﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Paramore.Brighter;
using SimpleInjector;

namespace Delikasee.Core.Infrastructure.SimpleInjectorExt.Brighter
{
    public static class BrighterContainerHelper
    {
        public static void SetupBrighter(this Container container, Assembly aggregateAssembly, Assembly commandAssembly)
        {
            var handlerFactory = new SimpleInjectorHandlerFactoryAsync(container);
            container.Register(typeof(IHandleRequestsAsync<>), aggregateAssembly);

            var subscriberRegistry = new SubscriberRegistry();
            subscriberRegistry.RegisterSubscribers(commandAssembly);

            var builder = CommandProcessorBuilder.With()
                .Handlers(new HandlerConfiguration(subscriberRegistry, handlerFactory))
                .DefaultPolicy()
                .NoTaskQueues()
                .RequestContextFactory(new InMemoryRequestContextFactory());

            container.RegisterInstance<IAmACommandProcessor>(builder.Build());
        }

        private static void RegisterSubscribers(this SubscriberRegistry subscriberRegistry, Assembly commandAssembly)
        {
            var commandTypes = commandAssembly.GetTypes()
                .Where(t => typeof(IRequest).IsAssignableFrom(t));

            Dictionary<Type, Type> commandAndHandlers = new Dictionary<Type, Type>();

            foreach (var cmdType in commandTypes)
            {
                var handlers = AppDomain.CurrentDomain.GetAssemblies()
                    .Where(a => a.FullName.StartsWith("Delikasee"))
                    .SelectMany(a => a.GetTypes())
                    .Where(t => t.IsSubclassOfGenericWithTypeParam(cmdType))
                    .ToList();

                if (handlers.Count > 1)
#pragma warning disable S112 // General exceptions should never be thrown
                    throw new Exception("Only one handler can be registered to a command");
#pragma warning restore S112 // General exceptions should never be thrown

                var handler = handlers.FirstOrDefault();

                if (handler != null)
                    commandAndHandlers.Add(cmdType, handler);
            }

            var method = typeof(SubscriberRegistry).GetMethod("RegisterAsync");
            foreach (var commandType in commandAndHandlers.Keys)
            {
                var genericMethod = method.MakeGenericMethod(commandType, commandAndHandlers[commandType]);
                genericMethod.Invoke(subscriberRegistry, null);
            }
        }

        private static bool IsSubclassOfGenericWithTypeParam(this Type type, Type genericParamType)
        {
            if (genericParamType != null && genericParamType != typeof(object))
            {
                var genericTypeArguments = type.GetInterfaces()
                    .Where(t => t.IsGenericType)
                    .SelectMany(a => a.GenericTypeArguments);

                return genericTypeArguments.Contains(genericParamType);
            }
            return false;
        }
    }
}
