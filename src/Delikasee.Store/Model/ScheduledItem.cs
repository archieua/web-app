﻿using System;
using DDD;
using Delikasee.Store.Events;

namespace Delikasee.Store.Model
{
    public class ScheduledItem : AggregateBase
    {
        public Guid StoreId { get; private set; }
        public Guid ProductId { get; private set; }
        public DateTimeOffset DeliverOn { get; private set; }
        public SellingOption SellingOption { get; private set; }

        [Obsolete("Use constructor with parameter when creating a new domain object")]
        public ScheduledItem()
        {
            
        }

        public ScheduledItem(Guid itemId, Guid storeId, Guid productId)
        {
            RaiseEvent(new ScheduledItemCreated(itemId, storeId, productId));
        }

        public void Apply(ScheduledItemCreated e)
        {
            Id = e.AggregateId;
            StoreId = e.StoreId;
            ProductId = e.ProductId;
        }

        public void ResumeSelling()
        {
            RaiseEvent(new ScheduledItemSellingResumed(Id));
        }

        public void Apply(ScheduledItemSellingResumed e)
        {
            //change state here
        }

        public void StopSelling()
        {
            RaiseEvent(new ScheduledItemSellingStopped(Id));
        }

        public void Apply(ScheduledItemSellingStopped e)
        {
            //change state here
        }

        public override string Identifier => $"scheduled-item-{Id}";
    }
}
