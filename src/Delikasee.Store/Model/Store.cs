﻿using System;
using DDD;
using Delikasee.Store.Events;

namespace Delikasee.Store.Model
{
    public class Store : AggregateBase
    {
        public string Name { get; private set; }
        public Guid OwnerId { get; private set; }

        /// <summary>
        /// Use constructor with parameter when creating a new domain object
        /// This parameterless constructor is for the repository
        /// </summary>
        [Obsolete("Use constructor with parameter when creating a new domain object")]
        public Store()
        {

        }

        public Store(Guid storeId, Guid ownerId, string name)
        {
            RaiseEvent(new StoreCreated(storeId, ownerId, name));
        }

        public void Apply(StoreCreated e)
        {
            Id = e.AggregateId;
            OwnerId = e.OwnerId;
            Name = e.Name;
        }

        public void ChangeName(string name)
        {
            RaiseEvent(new StoreNameChanged(Id, name));
        }

        public void Apply(StoreNameChanged e)
        {
            Name = e.Name;
        }

        public override string Identifier => $"store-{Id}";
    }
}
