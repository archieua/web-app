﻿using System;
using DDD;

namespace Delikasee.Store.Events
{
    public class ScheduledItemCreated : BaseDomainEvent
    {
        public Guid StoreId { get; set; }
        public Guid ProductId { get; set; }

        public ScheduledItemCreated(Guid itemId, Guid storeId, Guid productId)
        {
            AggregateId = itemId;
            StoreId = storeId;
            ProductId = productId;
        }
    }
}
