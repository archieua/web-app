﻿using System;
using DDD;

namespace Delikasee.Store.Events
{
    public class StoreNameChanged : BaseDomainEvent
    {
        public string Name { get; set; }

        public StoreNameChanged(Guid id, string name)
        {
            AggregateId = id;
            Name = name;
        }
    }
}
