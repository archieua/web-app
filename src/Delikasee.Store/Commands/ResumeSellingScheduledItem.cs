﻿using System;
using Paramore.Brighter;

namespace Delikasee.Store.Commands
{
    public class ResumeSellingScheduledItem : IRequest
    {
        public Guid Id { get; set; }
        public Guid ItemId { get; set; }
    }
}
